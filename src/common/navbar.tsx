
import Navbar from 'react-bootstrap/Navbar';
import Container from 'react-bootstrap/Container';

const NavbarComponent = () => {
    return  <Navbar fixed="top" className="bg-body-tertiary">
                <Container>
                    <Navbar.Brand href="#home">Pizza Hub</Navbar.Brand>
                    <Navbar.Toggle />
                    <Navbar.Collapse className="justify-content-end">
                        <Navbar.Text>
                        Signed in as: <a href="#login"> User 1</a>
                        </Navbar.Text>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
}

export default NavbarComponent;