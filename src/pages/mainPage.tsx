import { Container,Row,Col, Card } from "react-bootstrap";
import Product from "../components/product";
import Cart from "../components/cart";
import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { Pizza } from "./Pizza";


const MainPage = () => {

    const [pizzas, setPizzas] = useState<Pizza[]>([]);
    const [cartItems, setCartItems] = useState<Pizza[]>([]);
    const [selectedSize, setSelectedSize] = useState('');
    const [selectedPrice, setSelectedPrice] = useState(0);


  useEffect(() => {
    fetchPizzas();
  }, []);

  const fetchPizzas = async () => {
    try {
      const response = await axios.get('http://localhost:8080/pizzas'); // Replace with your backend API endpoint
      setPizzas(response.data.Data);
    } catch (error) {
      console.error('Error fetching pizzas:', error);
    }
  };

  const addToCart = (pizza: Pizza, size: string, price: number) => {
    setSelectedSize(size);
    setSelectedPrice(price);
    setCartItems([...cartItems, pizza]);
  };



    return(
     <Container style={{marginTop:"100",paddingTop:"60px",backgroundColor:"#EEE"}}>
            <Row >
                <Col xs sm={8} lg={8}>
                    <h3>Pizza</h3>
                </Col>
                <Col  xs sm={4} lg={4}>
                    <div>
                       <h4>Information</h4>
                    </div>
                </Col>
                <Col xs sm={8} lg={8}>
                    <Card style={{backgroundColor:"#EEE"}}>
                    <Row>
              {pizzas.map(pizza => (
                <Col key={pizza.pizzaId}>
                  <Product key={pizza.pizzaId} pizza={pizza} addToCart={addToCart} />
                </Col>
              ))}
            </Row>
                    </Card>
                </Col>
                <Col  xs sm={4} lg={4}>
                    <Card  style={{backgroundColor:"#EEE"}}>
                        <Cart   cartItems={cartItems} selectedPrice={selectedPrice}  selectedSize={selectedSize}/>
                    </Card>
                </Col>
            </Row>
    </Container>
    );
}

export default MainPage;