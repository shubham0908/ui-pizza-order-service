import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import { Pizza } from '../pages/Pizza';
import React, { useEffect, useState } from 'react';

interface ProductProps {
  pizza: Pizza;
  addToCart: (pizza: Pizza, size: string, price: number) => void;
  
}

interface SizeOption {
  size: string;
  price: number;
}

const Product: React.FC<ProductProps> = ({ pizza , addToCart}) =>{
  const [selectedSize, setSelectedSize] = useState('');
  const [selectedPrice, setSelectedPrice] = useState(0);
 // const [sizeOptions, setSizeOptions] = useState<SizeOption[]>([]);




  const handleSizeSelection = (size: string ,price: number) => {    
    setSelectedSize(size);
    setSelectedPrice(price);
  };

 


  const handleAddToCart = () => {
    let price = 0;
    switch (selectedSize) {
      case 'Regular':
        price = pizza.priceRegularSize;
        
        break;
      case 'Medium':
        price = pizza.priceMediumSize;
        break;
      case 'Large':
        price = pizza.priceLargeSize;
        break;
      default:
        break;
    }

    addToCart(pizza, selectedSize, price);
    setSelectedSize('');
    setSelectedPrice(0);
  };

  const getSizeButtonVariant = (size: string) => {
    return selectedSize === size ? 'primary' : 'outline-primary';
  };

 

  return (
    <Card style={{ width: '15rem',margin:"10px" }}>
      <Card.Img variant="top" src = {pizza.imageUrl} />
      <Card.Body>
        <Card.Title>{pizza.name}</Card.Title>
        <Card.Text>
         {pizza.description}
        </Card.Text>
        <Card.Text>Type: {pizza.type}</Card.Text>
        {/* <Card.Text>Price (Regular): ${pizza.priceRegularSize}</Card.Text>
        <Card.Text>Price (Medium): ${pizza.priceMediumSize}</Card.Text>
        <Card.Text>Price (Large): ${pizza.priceLargeSize}</Card.Text> */}
           <div>
          <p>Select Size:</p>
          <div>
          
            <Button
               variant={getSizeButtonVariant('Regular')}
              onClick={() => handleSizeSelection('Regular', pizza.priceRegularSize)}
            >
             
              Regular - ${pizza.priceRegularSize}
            </Button>
            
            <Button
              variant={getSizeButtonVariant('Medium')}
              onClick={() => handleSizeSelection('Medium', pizza.priceMediumSize)}
            >
              Medium - ${pizza.priceMediumSize}
            </Button>
           
            <Button
              variant={getSizeButtonVariant('Large')}
              onClick={() => handleSizeSelection('Large', pizza.priceLargeSize)}
            >
              Large - ${pizza.priceLargeSize}
            </Button>
          </div>
        </div>
        <Button variant="primary" disabled={!selectedSize} onClick={handleAddToCart} >Add to cart</Button>
      </Card.Body>
    </Card>
  );
}

export default Product;