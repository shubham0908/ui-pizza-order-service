import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import ListGroup from 'react-bootstrap/ListGroup';
import { Pizza } from '../pages/Pizza';

interface CartProps{
  cartItems: Pizza[];
  selectedSize: string;
  selectedPrice: number;
}
const Cart : React.FC<CartProps>= ({ cartItems ,selectedSize,selectedPrice}) => {
  return (
     <Card style={{ margin: "10px"  } }>
       <Card.Body>
         <Card.Title>Your Order</Card.Title>
         
         {cartItems.length === 0 ? (
            <Card.Text>
              <svg xmlns="http://www.w3.org/2000/svg" width="130" height="130" fill="currentColor" className="bi bi-cart" viewBox="0 0 16 16">
             <path d="M0 1.5A.5.5 0 0 1 .5 1H2a.5.5 0 0 1 .485.379L2.89 3H14.5a.5.5 0 0 1 .491.592l-1.5 8A.5.5 0 0 1 13 12H4a.5.5 0 0 1-.491-.408L2.01 3.607 1.61 2H.5a.5.5 0 0 1-.5-.5zM3.102 4l1.313 7h8.17l1.313-7H3.102zM5 12a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm7 0a2 2 0 1 0 0 4 2 2 0 0 0 0-4zm-7 1a1 1 0 1 1 0 2 1 1 0 0 1 0-2zm7 0a1 1 0 1 1 0 2 1 1 0 0 1 0-2z" />
           </svg> 
              <h5>Cart is empty. Add Products</h5></Card.Text>
          ) : (
            <ul>
              {cartItems.map((item) => (
                <div key={item.pizzaId}>  
                
                <Card.Img variant="top" src = {item.imageUrl } style={{ width: "100px"  } } />
                <Card.Text>{item.name}</Card.Text>
                <Card.Text>{item.description}</Card.Text>  
                {selectedSize && (
                  <Card.Text>
                     {selectedSize}
                  </Card.Text>
                )}

                </div>
              ))}
            </ul>
          )}


         <div>
         
           <ListGroup className="list-group list-group-flush">
             <ListGroup.Item as="li" >Min. Order:</ListGroup.Item>
             <ListGroup.Item as="li">Delivery Charge:</ListGroup.Item>
             <ListGroup.Item as="li" >I have coupon:</ListGroup.Item>
           </ListGroup>
         </div>
         <Button style={{ marginTop: "20px" }} variant="primary">Create Order</Button>
       </Card.Body>
     </Card>
   );
 }






 


 


//<div>
//{cartItems.length === 0 ? (
  // Cart empty content
  //<h5>Cart is empty. Add Products</h5>
//) : (
  // Cart items list content
  //<Card>
    //{cartItems.map((item) => (
      //<div key={item.pizzaId}>
        //<Card.Img variant="top" src = {item.imageUrl} />
        //<Card.Body>
        //<Card.Text>{item.name}</Card.Text>
        //<Card.Text>{item.description}</Card.Text>  
        //</Card.Body>
        //{/* Render other item details */}

     // </div>
    //))}
  //</Card>
//)}
//</div>
//);


export default Cart;