import './App.css';
import Content from './common/content';
import Footer from './common/footer';
import NavbarComponent from './common/navbar';
function App() {
  return (
    <div className="App" style={{backgroundColor:"#EEE"} } >
        <NavbarComponent/>
        <Content/>
        <Footer/>
    </div>
  );
}

export default App;
